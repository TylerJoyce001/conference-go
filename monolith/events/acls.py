import requests
from .keys import OPEN_WEATHER_API_KEY, PEXEL_API_KEY


def get_photo(city, state):
    url = "https://api.pexels.com/v1/search"
    params = {"per_page": 1, "query": f"{city} {state}"}
    headers = {"Authorization": PEXEL_API_KEY}
    res = requests.get(
        url,
        params=params,
        headers=headers,
    )
    print(res.json()["photos"][0]["src"]["original"])
    return res.json()["photos"][0]["src"]["original"]


#     # image_url = res.json()
#     # content["image_url"] = image_url


def get_coord(city, state):
    url = f"http://api.openweathermap.org/geo/1.0/direct?q={city},{state},USA&limit=1&appid={OPEN_WEATHER_API_KEY}"
    res = requests.get(url)
    return {"lat": res.json()[0]["lat"], "lon": res.json()[0]["lon"]}


def get_weather(lat, lon):
    url = f"https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={OPEN_WEATHER_API_KEY}"
    res = requests.get(url)
    response = {
        "description": res.json()["weather"][0]["description"],
        "current_temp": res.json()["main"]["temp"],
    }

    return response
    # weather_description = res.json()["main"]
    # current_temp = res.json()["weather"][0]["description"]
